export class AuthenticatedUser {
    id: number;
    username: string;
    password: string;
    firstName: string;
    fullName: string;
    email: string;
    authdata?: string;
}