import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseUrl = `${environment.apiUrl}/api`;
  private getUrl = `${this.baseUrl}/v1/users`;

  constructor(private _httpClient: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this._httpClient.get<User[]>(this.getUrl).pipe(
      map(response => response)
    );
  };

  getUser(id: number): Observable<User> {
    return this._httpClient.get<User>(`${this.getUrl}/${id}`).pipe(
      map(response => response)
    );
  };

  saveUser(user: User): Observable<User> {
    return this._httpClient.post<User>(this.getUrl, user);
  }

  searchUsers(query: string):Observable<User[]> {
    return this._httpClient.get<User[]>(`${this.getUrl}?q=${query}`).pipe(
      map(response => response)
    );
  };
}
