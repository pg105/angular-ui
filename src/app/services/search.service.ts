import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private eventSource = new BehaviorSubject('');
  searchQuery = this.eventSource.asObservable();

  constructor() { }

  broadcastSearch(searchQuery: string) {
    this.eventSource.next(searchQuery)
  }
}
