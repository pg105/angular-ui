import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service'
import { SearchService } from 'src/app/services/search.service'
import { User } from 'src/app/models/user';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewUserComponent } from '../view-user/view-user.component';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  users: User[] = [];
  constructor(private userService: UserService,
    private modalService: NgbModal,
    private searchService: SearchService) { }

  ngOnInit(): void {
    this.searchService.searchQuery.subscribe(searchQuery => {
      this.userService.searchUsers(searchQuery).subscribe(data =>
        this.users = data
      );
    });
    // this.getUsers();
  }

  getUsers() {
    this.userService.getUsers().subscribe(data => 
      this.users = data
    );
  }

  open(user: User) {
    const ref = this.modalService.open(ViewUserComponent, { size: 'lg' });
    ref.componentInstance.user = user;
  }
}
