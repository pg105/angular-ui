import { Component, OnInit } from '@angular/core';
import { SearchService } from 'src/app/services/search.service';
import { AuthenticationService } from '@app/services';
import { AuthenticatedUser } from '@app/models';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  filters = {
    keyword: ''
  };
  user: AuthenticatedUser;
  public isMenuCollapsed = true;
  constructor(private searchService: SearchService,
    private authenticationService: AuthenticationService) {
      this.authenticationService.user.subscribe(x => this.user = x);
  }

  ngOnInit(): void {
  }

  searchUser() {
    this.searchService.broadcastSearch(this.filters.keyword.toLowerCase())
  }

  logout() {
    this.authenticationService.logout();
}
}
