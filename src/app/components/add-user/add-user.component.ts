import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  user: User;
  constructor(
    private addUserService: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.user = new User();
    const isIdAvaiable = this.activatedRoute.snapshot.paramMap.has('id');
    if(isIdAvaiable) {
      const id = +this.activatedRoute.snapshot.paramMap.get('id');
      this.addUserService.getUser(id).subscribe(
        response => {
          this.user = response;
        }
      );
    }
  }

  saveUser() {
    this.addUserService.saveUser(this.user).subscribe(
      response => {
        this.router.navigate(["/users"]);
      }
    );
  }
}
